local mappings = {
  n = {
    L = { function() require("astronvim.utils.buffer").nav(vim.v.count > 0 and vim.v.count or 1) end, desc = "Next buffer" },
    H = { function() require("astronvim.utils.buffer").nav(-(vim.v.count > 0 and vim.v.count or 1)) end, desc = "Previous buffer" },
    ['<Up>'] = { 'gk' },
    ['<Down>'] = { 'gj' },
    ["<leader>C"] = { function()
      local buffer = require("astronvim.utils.buffer")
      local last_file = vim.fn.expand('%')
      buffer.close_all()
      vim.cmd.e(last_file)
      buffer.close_left()
    end, desc = "Close all buffers" },
    ["<leader>fs"] = {
      function()
        require("telescope.builtin").current_buffer_fuzzy_find()
      end, desc = "Find in current buffer"
    },
    ["<leader>r"] = { ":set rnu!<CR>", desc = "Toggle relativenumber" },
    ["\\"] = { ":ToggleTerm direction=float<CR>" },
    ["<leader>lce"] = { ":set spelllang=en<CR>", desc = "Set language to English" },
    ["<leader>lcs"] = { ":set spelllang=es<CR>", desc = "Set language to Spanish" },
    ["<leader>lct"] = { ":set spell!<CR>", desc = "Toggle Spell Checker" },
    ["<leader>lcc"] = { "1z=", desc = "Correct spelling mistake" },
    ["<leader>lcn"] = { "]sz=", desc = "Fix next spelling mistake" },
    ["<leader>lcN"] = { "[sz=", desc = "Fix previous spelling mistake" },
    ["gs"] = { "]s", desc = "Go to next spelling mistake" },
    ["gS"] = { "[s", desc = "Go to previous spelling mistake" },
    ["<leader>W"] = { ":SudaWrite<CR>:e %<CR>", desc = 'Save as sudo' },
    ["<C-s>"] = { function() vim.api.nvim_command('write') end, desc = 'Save File' },
    ["<leader>uz"] = { ":ZenMode<CR>", desc = 'Toggle Zen mode' },
    ["<leader>ul"] = { function()
      if (vim.o.background == 'dark') then
        vim.o.background = 'light'
      else
        vim.o.background = 'dark'
      end
    end, desc = 'Toggle light mode' },
    ["<leader>bb"] = { "<cmd>tabnew<cr>", desc = "New tab" },
    ["<leader>bc"] = { "<cmd>BufferLinePickClose<cr>", desc = "Pick to close" },
    ["<leader>bj"] = { "<cmd>BufferLinePick<cr>", desc = "Pick to jump" },
    ["<leader>bt"] = { "<cmd>BufferLineSortByTabs<cr>", desc = "Sort by tabs" },

    ["<leader>D"] = { "<cmd>DBUIToggle<cr>", desc = "Toggle DBUI" },

    -- Prefixes
    ["<leader>b"] = { name = "Buffer" },
    ["<leader>lc"] = { name = "Spell Checker" },
  },
  i = {
    ["<Down>"] = { "<ESC>gja" },
    ["<Up>"] = { "<ESC>gka" },
    ["jj"] = { "<ESC>l" },
    ["<C-s>"] = { function() vim.api.nvim_command('write') end, desc = 'Save File' },
  }
}

return mappings
