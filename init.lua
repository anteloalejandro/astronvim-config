return {
  -- Configure AstroNvim updates
  updater = {
    remote = "origin",     -- remote to use
    channel = "stable",    -- "stable" or "nightly"
    version = "latest",    -- "latest", tag name, or regex search like "v1.*" to only do updates before v2 (STABLE ONLY)
    branch = "nightly",    -- branch name (NIGHTLY ONLY)
    commit = nil,          -- commit hash (NIGHTLY ONLY)
    pin_plugins = nil,     -- nil, true, false (nil will pin plugins on stable only)
    skip_prompts = false,  -- skip prompts about breaking changes
    show_changelog = true, -- show the changelog after performing an update
    auto_quit = false,     -- automatically quit the current session after a successful update
    remotes = {            -- easily add new remotes to track
      --   ["remote_name"] = "https://remote_url.come/repo.git", -- full remote url
      --   ["remote2"] = "github_user/repo", -- GitHub user/repo shortcut,
      --   ["remote3"] = "github_user", -- GitHub user assume AstroNvim fork
    },
  },
  -- Set colorscheme to use
  colorscheme = "gruvbox",
  -- Diagnostics configuration (for vim.diagnostics.config({...})) when diagnostics are on
  diagnostics = {
    virtual_text = true,
    underline = true,
  },
  lsp = {
    -- customize lsp formatting options
    formatting = {
      -- control auto formatting on save
      format_on_save = {
        enabled = true,     -- enable or disable format on save globally
        allow_filetypes = { -- enable format on save for specified filetypes only
          -- "go",
        },
        ignore_filetypes = { -- disable format on save for specified filetypes
          -- "python",
        },
      },
      disabled = { -- disable formatting capabilities for the listed language servers
        -- "sumneko_lua",
      },
      timeout_ms = 1000, -- default format timeout
      -- filter = function(client) -- fully override the default formatting function
      --   return true
      -- end
    },
    -- enable servers that you already have installed without mason
    servers = {
      -- "pyright"
    },

    -- require('lspconfig').gdscript.setup {
    --   on_attach = nil
    -- },

    config = {
      tsserver = {
        single_file_support = true,
        -- root_dir = function() return vim.loop.cwd() end
      },
      intelephense = {
        single_file_support = true
      },
      emmet_ls = {
        single_file_support = true,
        filetypes = {
          'typescriptreact', 'javascriptreact',
          'sass', 'scss', 'less', 'xml', 'html',
          'html.handlebars', 'jst', 'aspx', 'mason'
        }
      },
      --[[ astro = {
        single_file_support = true,
        filetypes = { 'astro', 'html' },
        root_dir = function() return vim.loop.cwd() end
      }, ]]
      html = {
        single_file_support = true,
        filetypes = { 'html', 'html.handlebars', 'blade' },
        init_options = {
          configurationSection = { "html", "css", "javascript" },
          embeddedLanguages = {
            css = true,
            javascript = true
          },
          provideFormatter = true
        }
      },
      sqlls = {
        single_file_support = true
      },
      --[[ csharp_ls = {
        single_file_support = true
      } ]]
      -- omnisharp = {
      --   on_attach = function(client, bufnr)
      --     client.server_capabilities.semanticTokensProvider = {
      --       full = vim.empty_dict(),
      --       legend = {
      --         tokenModifiers = { "static_symbol" },
      --         tokenTypes = {
      --           "comment",
      --           "excluded_code",
      --           "identifier",
      --           "keyword",
      --           "keyword_control",
      --           "number",
      --           "operator",
      --           "operator_overloaded",
      --           "preprocessor_keyword",
      --           "string",
      --           "whitespace",
      --           "text",
      --           "static_symbol",
      --           "preprocessor_text",
      --           "punctuation",
      --           "string_verbatim",
      --           "string_escape_character",
      --           "class_name",
      --           "delegate_name",
      --           "enum_name",
      --           "interface_name",
      --           "module_name",
      --           "struct_name",
      --           "type_parameter_name",
      --           "field_name",
      --           "enum_member_name",
      --           "constant_name",
      --           "local_name",
      --           "parameter_name",
      --           "method_name",
      --           "extension_method_name",
      --           "property_name",
      --           "event_name",
      --           "namespace_name",
      --           "label_name",
      --           "xml_doc_comment_attribute_name",
      --           "xml_doc_comment_attribute_quotes",
      --           "xml_doc_comment_attribute_value",
      --           "xml_doc_comment_cdata_section",
      --           "xml_doc_comment_comment",
      --           "xml_doc_comment_delimiter",
      --           "xml_doc_comment_entity_reference",
      --           "xml_doc_comment_name",
      --           "xml_doc_comment_processing_instruction",
      --           "xml_doc_comment_text",
      --           "xml_literal_attribute_name",
      --           "xml_literal_attribute_quotes",
      --           "xml_literal_attribute_value",
      --           "xml_literal_cdata_section",
      --           "xml_literal_comment",
      --           "xml_literal_delimiter",
      --           "xml_literal_embedded_expression",
      --           "xml_literal_entity_reference",
      --           "xml_literal_name",
      --           "xml_literal_processing_instruction",
      --           "xml_literal_text",
      --           "regex_comment",
      --           "regex_character_class",
      --           "regex_anchor",
      --           "regex_quantifier",
      --           "regex_grouping",
      --           "regex_alternation",
      --           "regex_text",
      --           "regex_self_escaped_character",
      --           "regex_other_escape",
      --         },
      --       },
      --       range = true,
      --     }
      --   end,
      --   showOmnisharpLogOnError = false,
      --   single_file_support = true
      -- }
    },
  },
  -- Configure require("lazy").setup() options
  lazy = {
    defaults = { lazy = true },
    performance = {
      rtp = {
        -- customize default disabled vim plugins
        disabled_plugins = { "tohtml", "gzip", "matchit", "zipPlugin", "netrwPlugin", "tarPlugin" },
      },
    },
  },
  -- This function is run last and is a good place to configuring
  -- augroups/autocommands and custom filetypes also this just pure lua so
  -- anything that doesn't fit in the normal config locations above can go here
  polish = function()
    require('user.autocmd')

    if vim.g.neovide then
      -- NEOVIDE ZOOM
      vim.g.neovide_scale_factor = 1.0
      local change_scale_factor = function(delta)
        vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta
      end
      vim.keymap.set("n", "<C-+>", function()
        if vim.g.neovide_scale_factor < 2.0 then
          change_scale_factor(1.25)
        end
      end)
      vim.keymap.set("n", "<C-->", function()
        if vim.g.neovide_scale_factor > 0.5 then
          change_scale_factor(1/1.25)
        end
      end)
    end
  end,
}
