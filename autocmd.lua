local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

-- Global changes
augroup("global", { clear = true })

-- Filetype dependant format changes
augroup("filetype_conf", { clear = true })
autocmd("BufEnter", {
  desc = "Use tabs instead of spaces for the specified filetypes",
  group = "filetype_conf",
  pattern = { "*.gd", "*.md", "*.tex" },
  callback = function()
    vim.cmd("setlocal noexpandtab")
  end
})
autocmd("BufEnter", {
  desc = "Disable number column for the specified filetypes",
  group = "filetype_conf",
  pattern = { "*.md", "*.tex" },
  callback = function()
    vim.cmd("setlocal nonu")
  end
})
autocmd("BufEnter", {
  desc = "Enable text wrap for the specified filetypes",
  group = "filetype_conf",
  pattern = { "*.md", "*.tex" },
  callback = function()
    vim.cmd("setlocal wrap")
  end
})
autocmd("BufEnter", {
  desc = "Enable spelling for the specified filetypes",
  group = "filetype_conf",
  pattern = { "*.md", "*.tex" },
  callback = function()
    vim.cmd("setlocal spell")
  end
})
