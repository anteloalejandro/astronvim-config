return {
  opt = {
    relativenumber = true,
    number = true,
    spell = false,
    spelllang = "en",
    signcolumn = "auto",
    wrap = false,
    linebreak = true,
    softtabstop = 2,
    shiftwidth = 2,
    expandtab = true,
    cmdheight = 1,

    -- NEOVIDE SPECIFIC CONFIG
    guifont = "UbuntuMono Nerd Font:h15:#e-subpixelantialias"
  },
  g = {
    mapleader = " ",
    autoformat_enabled = false,
    cmp_enabled = true,
    autopairs_enabled = true,
    diagnostics_enabled = true,
    status_diagnostics_enabled = true,
    icons_enabled = true,
    ui_notifications_enabled = true,

    -- NEOVIDE SPECIFIC CONFIG
    neovide_cursor_vfx_mode = "",
    neovide_cursor_trail_size = 0.1
  },
}
