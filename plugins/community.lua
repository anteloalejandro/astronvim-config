return {
  "AstroNvim/astrocommunity",
  { import = "astrocommunity.colorscheme.gruvbox-nvim" },
  -- { import = "astrocommunity.colorscheme.catppuccin" },
  -- { import = "astrocommunity.completion.copilot-lua-cmp" },
  -- { import = "astrocommunity.pack.cs" },
  { import = "astrocommunity.pack.typescript" },
  { import = "astrocommunity.pack.astro" },
  -- { import = "astrocommunity.pack.bash" },
  -- { import = "astrocommunity.pack.cmake" },
  { import = "astrocommunity.pack.angular" },
  { import = "astrocommunity.pack.php" },
  -- { import = "astrocommunity.pack.json" },
  { import = "astrocommunity.pack.html-css" },
  -- { import = "astrocommunity.pack.python" },
  -- { import = "astrocommunity.pack.cpp" },
  { import = "astrocommunity.pack.lua" },
  { import = "astrocommunity.pack.java" }
}
